
---
## Introducción

Plantilla de diseño de niveles realizada en Unreal Engine orientada a la realización de trabajos académicos.

!!! Warning
    Las imágenes de la interfaz corresponden a la versión para UE4, pero los atributos y el comportamiento en UE5 es similar.

Repositorio del proyecto: [https://gitlab.com/antonioseoane/DNTPLevel](https://gitlab.com/antonioseoane/DNTPLevel)

(c) 2020-23 Antonio Seoane ([linkedin.com/in/antonioseoane](linkedin.com/in/antonioseoane))

_Este plantilla está bajo una licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional._

<figure markdown>
  ![Plantilla de diseño de niveles](imgs_index/plantilla_01.jpg){ width="" }
  <figcaption>Plantilla de diseño de niveles.</figcaption>
</figure>

<br>

---
## Créditos

__Desarrollo de la plantilla:__ Antonio Seoane ([linkedin.com/in/antonioseoane](linkedin.com/in/antonioseoane)).

__Motor de videojuegos:__ Unreal Engine 5 de Epic Games.

__Paquetes externos empleados:__

* [Advanced Locomotion System V4](https://www.unrealengine.com/marketplace/en-US/product/advanced-locomotion-system-v1) de LongmireLocomotion disponible en el Unreal Engine Marketplace
* [Realistic Starter VFX Pack Vol 2
](https://www.unrealengine.com/marketplace/en-US/product/realistic-starter-vfx-pack-vol) de FX Cat UA disponible en el Unreal Engine Marketplace
* [Niagara Mega VFX Pack vol. 1](https://www.unrealengine.com/marketplace/en-US/product/niagara-mega-vfx-pack-vol) de Suppart disponible en el Unreal Engine Marketplace
* Weapon Component de Quick and Easy (actualmente no disponible)
* [Creating a Level Blockout for Game Development](
https://unrealengine.com/marketplace/en-US/product/creating-a-level-blockout) de Unreal Online Learning disponible en el Unreal Engine Marketplace
* [Textura "bullet hole decal"](https://opengameart.org/content/bullet-decal) de musdasch disponible en [OpenGameArt.org](http://OpenGameArt.org)

<br>

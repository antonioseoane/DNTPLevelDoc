## Materiales para blockout

Para hacer un whitebox o blockout se pueden utilizar materiales lisos con una grid de referencia de las métricas del mundo de juego.

Los materiales de blockout utilizados en la plantilla se encuentran en:

```
Content\BlockingMaterials\
```

Hay dos tipos de materiales:

- __Material__ o materiales base.
- __MaterialInstance__ o materiales derivados de los materiales base que solo permite modificar sus parámetros.

En la plantilla hay dos materiales base:

- __M_LDGrid_Base__ que solo permite mapeado en coordenadas UV asociadas a la mesh. Este es el material base recomendado para estimar el texel density en pantalla de un objeto en relación a su mapeado UV para estimar la resolución de textura adecuada.
- __M_LDGrid_Local__ que permite mapeado en coordenadas locales del objeto o en espacio mundo. Este es el material base recomendado para usar de referencia para calcular medidas.

Para crear un nuevo material se recomienda copiar una instancia ya existente o crear un __Material Instance__ a partir de __M_LDGrid_Local__.

<figure markdown>
  ![Materials](imgs_blockingmaterials/mats_01.jpg){ width="" }
  <figcaption>Materiales de la plantilla.</figcaption>
</figure>

<br>

---
## Atributos de las _Material Instances_ derivadas de __M_LDGrid_Local__

El material __M_LDGrid_Local__ utiliza un color base con dos tonos para crear un grid y después genera una cuadrícula con líneas a cada metro de distancia. Se puede generar una segunda cuadrícula con líneas ubicadas a la distancia que deseemos.

Todas las instancias derivadas de __M_LDGrid_Local__ tendrán los siguientes atributos modificables:

<figure markdown>
|Atributo|Descripción|Valores|
|--|--|:--:|
|__Base Color__|Color base de la grid.|Color|
|__Grid Glow__|Intensidad de las líneas situadas a cada metro de distancia1.|Número flotante|
|__Line Color__|Color de las líneas de la cuadrícula generada.|Color|
|__Line Spacing__|Espaciado de la segunda cuadrícula generada en cm.|Número flotante|
|__Local Space__|Indica si se utilizarán las coordenadas UV locales o se calcularán en world space.|Si/No|
</figure>

<figure markdown>
  ![Materials](imgs_blockingmaterials/mats_04.jpg){ width="" }
  <figcaption>Parámetros de las Material Instances.</figcaption>
</figure>

<br>

---
## Local vs World Space

En el caso de que esté activo el mapeado en __Local Space__ se utilizarán las coordenadas UV del objeto. Esto implica que:

- Si el objeto tiene las UVs distorsionadas, se verá la grid distorsionada.
- Si el objeto se escala, se escalará la grid.
- Si el objeto se mueve, no variará el mapeado de la grid.

<figure markdown>
  ![Materials](imgs_blockingmaterials/mats_02.jpg){ width="" }
  <figcaption>Materiales de blockout en local space.</figcaption>
</figure>

En por el contrario no está activo el mapeado en _Local Space_ entonces el mapeado se realizará en __World Space__ por lo que las coordenadas UV del objeto se calcularán en función de la posición en el mundo de cada vértice. Esto implica que:

- No se utilizarán las UVs del objeto.
- Nunca aparecerá la grid distorsionada.
- Si el objeto se mueve, se modifica el mapeado de la grid, que siempre se ajustará al espacio mundo, tal como se ve en la siguiente imagen:

<figure markdown>
  ![Materials](imgs_blockingmaterials/mats_03.jpg){ width="" }
  <figcaption>Materiales de blockout en world space.</figcaption>
</figure>



<br>




Las barreras se utilizarán para evitar que el personaje se caiga de los bordes elevados del escenario o para bloquear al jugador para no permitirle acceder a las zonas no transitables.

Para ubicar las barreras simplemente se utilizan __BlockingVolumes__ que se encuentran entre los actores por defecto de Unreal Engine.

!!! Note
	Para ubicar las barrearas de manera adecuada, se debe de tener en cuenta las métricas del juego como la altura a la que puede saltar o escalar el jugador o el tamaño de su volumen de colisión.

En el nivel demo de la plantilla podemos encontrar un ejemplo del uso de las barreras para evitar que el jugador se caiga de la zona de escaleras:

<figure markdown>
  ![Barreras](imgs_barreras/barreras_01.jpg){ width="" }
  <figcaption>Barreras con BlockingVolumes.</figcaption>
</figure>

<br>

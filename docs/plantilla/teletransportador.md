## Los actores del teletransportador

El __teletransportador__ es una mecánica que utiliza dos actores, uno de __origen__ y uno de __destino__. Cuando el personaje del jugador colisiona con el origen es teletransportado al destino.

Al actor origen tiene que estar asociado a un actor destino para poder hacer el salto.

Una vez que se produce el teletransporte, el jugador queda mirando en la dirección que indica una flecha verde del actor teletransportador destino.

Colocaremos dos actores en la escena, el actor origen y el destino. Una vez colocados los asociaremos asignando el actor destino al atributo __Destination__ del actor origen.

!!! Tip
	Un actor origen solo puede tener una referencia destino, pero podemos saltar al mismo destino desde distintos puntos de origen asociando el mismo actor destino.

El volumen del teletransportador origen se puede escalar para ajustar el volumen de colisión con el jugador.

Podemos encontrar los actores en:

```
Content\Mechanics\TeleportOrigin_BP.uasset
Content\Mechanics\TeleportDestination_BP.uasset
```

<figure markdown>
  ![Actores del teletransportador](imgs_teletransportador/teleport_01.jpg){ width="" }
  <figcaption>Actores del teletransportador.</figcaption>
</figure>

<br>

---

## Atributos del teletransportador origen

El actor __teletransportador origen__ tiene los siguientes atributos que se encuentran en la sección __Default__:


<figure markdown>
|Atributo|Descripción|Valores|
|--|--|:--:|
|__Destination__|Actor teletransportador destino al que saltará una vez que se colisiones con el origen.|Referencia a un Actor|
</figure>

<figure markdown>
  ![Atributos de un teletransportador origen](imgs_teletransportador/teleport_02.png){ width="" }
  <figcaption>Atributos de un teletransportador origen.</figcaption>
</figure>

<br>

---

## El teletransportador destino

En el teletransportador destino se indica mediante una flecha la dirección en la que quedará mirando el jugador una vez realizado el teletransporte.

<figure markdown>
  ![Teletransportador destino](imgs_teletransportador/teleport_03.jpg){ width="" }
  <figcaption>Flecha de dirección del teletransportador destino.</figcaption>
</figure>


<br>

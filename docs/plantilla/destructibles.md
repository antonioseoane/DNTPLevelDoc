
## El actor destructible

El __destructible__ es un actor que se destruye cuando recibe el daño suficiente de los disparos del jugador. Lo encontramos en:

```
Content\Mechanics\Destructible_BP.uasset
```

<figure markdown>
  ![Actores destructibles](imgs_destructibles/destructibles_01.jpg){ width="400" }
  <figcaption>Actores destructibles.</figcaption>
</figure>

<br>

---

## Atributos

Los __destructibles__ tienen los siguientes atributos que se encuentran en la sección __Default__:


<figure markdown>
|Atributo|Descripción|Valores|
|--|--|:--:|
|__Color__|Color del objeto, no se debe de cambiar el material.|Color|
|__Health__|Vida del actor, desaparece cuando llega a 0. |Mayor o igual a 0.|
|__Explosion__|Indica si queremos que haya un efecto de explosión al desaparecer el objeto.|SI/NO|
</figure>

<figure markdown>
  ![Atributos de un destructible](imgs_destructibles/destructibles_02.png){ width="" }
  <figcaption>Atributos de un destructible.</figcaption>
</figure>


!!! Tip
	¿Se puede cambiar la figura? Si, se puede cambiar modificando el Mesh que se encuentra dentro del blueprint.

<br>

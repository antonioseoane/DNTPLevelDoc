
## El actor coleccionable

El __coleccionable__ es un actor que se desaparece cuando el jugador colisiona con él. Lo encontramos en:

```
Content\Mechanics\Collectible_BP.uasset
```

<figure markdown>
  ![Actores coleccionables](imgs_coleccionables/collectibles_01.jpg){ width="400" }
  <figcaption>Actores coleccionables.</figcaption>
</figure>

<br>

---

## Atributos

Los __coleccionables__ tienen los siguientes atributos que se encuentran en la sección __Default__:


<figure markdown>
|Atributo|Descripción|Valores|
|--|--|:--:|
|__Color__|Color del objeto, no se debe de cambiar el material.|Color|
</figure>

<figure markdown>
  ![Atributos de un destructible](imgs_coleccionables/collectibles_02.png){ width="" }
  <figcaption>Atributos de un destructible.</figcaption>
</figure>

<br>

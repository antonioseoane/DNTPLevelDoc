
## Evento de nivel

El mapa demo de la plantilla incluye un ejemplo de un evento de nivel que se divide en tres partes.

<figure markdown>
  ![Eventos](imgs_eventos/event_01.jpg){ width="700" }
  <figcaption>Ejemplo de evento de nivel.</figcaption>
</figure>

El evento se compone de las siguientes etapas:

1. Derrumbe de un puente al pasar por encima.
2. Puerta que se abre de manera automática.
3. Derrumbe de rocas que caen cerca del jugador.

Cada etapa está programada mediante un __TriggerVolume__ que al colisionar lanza una __LevelSequence__, tal como se ve en la siguiente imagen:

<figure markdown>
  ![Eventos](imgs_eventos/event_02a.jpg){ width="700" }
  <figcaption>Evento de nivel.</figcaption>
</figure>

El proceso seguido para la creación de cada etapa consiste en:

1. Creación de una __LevelSequence__ a partir de las herramientas para realizar cinemáticas.
2. Añadir objetos y atributos afectados a la secuencia.
3. Crear animación, ajustar claves y corregir curvas de animación.
4. Añadir un __TriggeredVolume__ en el nivel, ajustar su tamaño y añadir en el blueprint del nivel un evento __OnActorBeginOverlap__.
5. Desde el evento creado lanzar la secuencia y borrar el __TriggeredVolume__ para evitar que se repita el evento.

En el Punto 5 hay que tener cuidado con ejecutar la secuencia desde el principio y dejarla pausada al final. Por esta razón se utilizarán los siguientes parámetros al lanzar la secuencia:

<figure markdown>
  ![Eventos](imgs_eventos/event_06.png){ width="" }
  <figcaption>Evento de lanzamiento de la secuencia dentro del Level Blueprint.</figcaption>
</figure>

!!! Warning
	Las __Level Sequences__ usan referencias a actores de un nivel, no se pueden cambiar de nivel ni reusar en distintos niveles. Hay que tener mucho cuidado para evitar la pérdida de dichas referencias cuando se hacen copias de un nivel ya que se pueden perder dichas referencias a los objetos afectados y habrá que reasignarlas.

<br>

---

## 1. Derrumbe de un puente al pasar por encima

En esta secuencia se animan manualmente tres piezas del puente que se caen de manera progresiva animando su traslación.

<figure markdown>
  ![Eventos](imgs_eventos/event_03.jpg){ width="" }
  <figcaption>Secuencia de derrumbe del puente.</figcaption>
</figure>

<br>

---

## 2. Puerta que se abre de manera automática

En esta secuencia se animan manualmente las dos hojas de una puerta que se abren de manera sincronizada animando su traslación.

<figure markdown>
  ![Eventos](imgs_eventos/event_04.jpg){ width="" }
  <figcaption>Secuencia de apertura de la puerta.</figcaption>
</figure>

<br>

---

## 3. Derrumbe de rocas que caen cerca del jugador

En esta secuencia se anima la simulación de las físicas de las rocas que caen sobre el jugador.

<figure markdown>
  ![Eventos](imgs_eventos/event_05.jpg){ width="" }
  <figcaption>Secuencia de caída de rocas.</figcaption>
</figure>


<br>

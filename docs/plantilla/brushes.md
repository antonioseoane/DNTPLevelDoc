
Los __brushes__ se utilizan para crear figuras simples para la realización de un _blockout_ o _whitebox_. Estos brushes se pueden editar vértice a vértice, se pueden modificar mediante operadores como la extrusión, o combinar mediante operaciones booleanas para crear figuras complejas.

En el mapa de ejemplo de la plantilla se muestra un ejemplo del uso de un brush aditivo y 2 sustractivos para la creación de una figura compleja.

<figure markdown>
  ![Brushes](imgs_brushes/brushes_01.jpg){ width="700" }
  <figcaption>Ejemplo de brushes aditivos y sustractivos.</figcaption>
</figure>

<br>

!!! Tip
	Se puede modificar el material del brush en los _Details_ del actor, pero cuando se selecciona un brush en el viewport, solo se selecciona una cara y si selecciona el brush en el outliner no permite cambiar el material.
	Para poder cambiar el material a todo el brush, se puede seleccionar una cara del brush dentro del viewport y después con __SHIFT+B__ seleccionar todas las caras restantes de manera automática.

!!! Note
	Un actor se puede convertir a __StaticMesh__ pero al hacer esto se creará un objeto nuevo en el _Content_ que normalmente no tendrá las colisiones bien definidas. Se deberán editar las colisiones de dicho objeto para su correcto funcionamiento.

<br>

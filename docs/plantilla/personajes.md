## El actor personaje

El actor del personaje de la plantilla usa el blueprint __ALS_Antonio_AnimMan_CharacterBP__. Este actor gestiona el comportamiento de los personajes y de la cámara asociada al personaje del jugador. Lo encontramos en:

```
Content\Blueprints\ALSV4_Antonio\Blueprints\CharacterLogic\ALS_Antonio_AnimMan_CharacterBP.uasset
```

Este actor es el mismo que se utilizará para:

- Personaje Jugador
- Enemigos
- Otros NPCs

Características de los personajes:

- Pueden andar, correr, esprintar, saltar, subirse a un lugar alto, agacharse, apuntar y disparar.
- Puede configurarse el color completo o por partes del juego.
- Puede configurarse el tipo de cuerpo, hombre o mujer.
- Puede configurarse la salud del personaje.
- Puede equipar un rifle o una pistola a dos manos.
- En el caso del personaje del jugador el rifle dispara ráfagas mientras que la pistola realiza disparos individuales.
- En el caso del personaje del jugador puede configurarse el daño que realiza al disparar.
- En el caso de los NPCs se puede configurar el comportamiento del movimiento y equipamiento de los personajes.

<figure markdown>
  ![Personajes](imgs_personajes/char_01b.jpg){ width="" }
  <figcaption>Personajes.</figcaption>
</figure>

<br>

---

## Atributos

Los personajes tienen los siguientes atributos que se encuentran en distintas secciones de los _Details_ del blueprint.

### Sección __Colors__

En esta sección encontraremos la configuración de colores y ropa del personaje.

<figure markdown>
|Atributo|Descripción|Valores|
|--|--|:--:|
|__Default Color__|Color que se usará para el personaje completo<br> con el modo _Solid Color_ activo.|Color|
|__Skin Color__|Color de la piel.|Color|
|__Shirt Color__|Color de la camiseta.|Color|
|__Pants Color__|Color de los pantalones.|Color|
|__Shoes Color__|Color de los zapatos.|Color|
|__Globes Color__|Color de los guantes.|Color|
|__Hair Color__|Color del pelo (solo si es un personaje femenino).|Color|
|__Shirt Type__|Tipo de camiseta.|Color|
|__Pants Type__|Tipo de pantalones.|Color|
|__Shoes__|Indica si el personaje lleva zapatos.|Si/No|
|__Globes__|Indica si el personaje lleva guantes.|Si/No|
|__Solid Color__|Indica si se utilizá un color único para todo el personaje. <br> En ese caso se utilizará el __Default Color__|Si/No|
</figure>

<figure markdown>
  ![Personajes](imgs_personajes/char_02b.jpg){ width="" }
  <figcaption>Configuración de la sección Colors.</figcaption>
</figure>


### Sección __Character Stats__

En esta sección encontraremos la configuración relativa a la vida, daño y tipo de cuerpo del personaje.

<figure markdown>
|Atributo|Descripción|Valores|
|--|--|:--:|
|__Health__|Salud del personaje. Éste morirá cuando llegue a 0.|Color|
|__Base Damage__|Daño que se realizará con cada disparo del arma equipada.|Color|
|__Body Type__|Tipo de cuerpo del personaje, hombre o mujer.|Male/Female|
</figure>

<figure markdown>
  ![Personajes](imgs_personajes/char_07b.jpg){ width="" }
  <figcaption>Configuración de la sección Character Stats.</figcaption>
</figure>


### Sección __Input__

En esta sección encontraremos la configuración relativa al tipo de comportamiento del personaje. Modificarlo solo para los NPCs y dejarlo con los valores por defecto para el personaje del jugador.

<figure markdown>
|Atributo|Descripción|Valores|
|--|--|:--:|
|__Desired Rotation Mode__|Indica si el personaje siempre va a orientarse en la dirección del jugador,<br>si va a mirar en la dirección de su movimiento<br>o si va a mirar y apuntar siempre hacia el jugador.|LookingDirection<br>VelocityDirection<br>Aiming|
|__Desired Gait__|Indica si el personaje se moverá corriendo,<br>andando<br>o esprintando.|Running<br>Walking<br>Sprinting|
|__Desired Stance__|Indica si el personaje se encuentra de pie<br>o agachado.|Standing<br>Crouching|
|__Auto Receive Input__|NO TOCAR, dejarlo en Disabled|Disabled<br>Player 0<br>Player 1<br>Player 2<br>...|
|__Input Priority__|NO TOCAR, dejarlo en 0|Entero|
</figure>

<figure markdown>
  ![Personajes](imgs_personajes/char_04b.jpg){ width="" }
  <figcaption>Configuración de la sección Input.</figcaption>
</figure>

### Sección __State Values__

En esta sección, el __Overlay State__ permite modificar el equipamiento del personaje. Solo debemos de hacerlo para los NPCs, ya que el personaje del jugador ya equipará las armas de manera automática a partir del esquema de control de la plantilla.

<figure markdown>
|Atributo|Descripción|Valores|
|--|--|:--:|
|__Overlay State__|Tipo de equipamiento que lleva el personaje. Default indica que no equipa ningún arma.<br>Rifle indica que el personaje equipará un rifle.<br>Pistol 2H indica que el personaje equipará una pistola a dos manos.<br>Utilizar solo los indicados en la tabla.|Default<br>Rifle<br>Pistol 2H|
</figure>

<figure markdown>
  ![Personajes](imgs_personajes/char_05b.jpg){ width="" }
  <figcaption>Configuración de la sección State Values.</figcaption>
</figure>


### Sección __Pawn__

Para asignar uno de los actores como el personaje del jugador debe de de asignarse el valor __Player 0__ al atributo __Auto Possess Player__ de la sección __Pawn__. Solo el personaje del jugador debe tener este valor, el resto deben de tener el valor por defecto.

<figure markdown>
  ![Personajes](imgs_personajes/char_06b.jpg){ width="" }
  <figcaption>Configuración de la sección Pawn.</figcaption>
</figure>


<br>

---
## Configuración del personaje del jugador

Añadir el actor __ALS_Antonio_AnimMan_CharacterBP__ a la escena o utilizar un actor ya añadido.

En los _Details_ del actor:

1. Configurar el aspecto del personaje cambiando los colores en la sección __Colors__ y el tipo de cuerpo en la sección __Character Stats__
2. Configurar el daño y la salud en la sección __Character Stats__.
3. Dejar las secciones __Input__ y __State Values__ con los valores por defecto.
4. En la sección __Pawn__, asignar el valor __Player 0__ al atributo __Auto Possess Player__. Solo el personaje del jugador debe tener este valor, el resto deben de tener el valor por defecto.



<br>

---
## Configuración de los enemigos

Añadir el actor __ALS_Antonio_AnimMan_CharacterBP__ a la escena o utilizar un actor ya añadido.

Si queremos que los actores se muevan por un área, debemos de añadir el o los __Nav Mesh Bounds Volumes__ necesarios y ajustarlos a las zonas navegables, de lo contrario se quedarán en una posición fija.

En los _Details_ del actor:

1. Configurar el aspecto del personaje cambiando los colores en la sección __Colors__ y el tipo de cuerpo en la sección __Character Stats__.
2. Configurar la salud del personaje en la sección __Character Stats__. Los enemigos no dispararán y no harán daño.
3. Configurar en la sección __Input__ el comportamiento del personaje.
	- __Desired Rotation Mode__:
		- _LookingDirection_: el personaje a mirar en la dirección del jugador. Cuando el ángulo es muy grande, moverá el cuerpo para poder seguir orientando la cabeza hacia el jugador.
		- _VelocityDirection_: el personaje va a mirar siempre en la dirección de su movimiento. Si no se mueve se quedará en una orientación fija.
		- _Aiming_: el personaje va a mirar y orientarse siempre hacia el jugador. En caso de llevar arma va a apuntar hacia el jugador.
	- __Desired Gait__:
		- _Running_: el personaje correrá todo el tiempo.
		- _Walking_: el personaje andará todo el tiempo.
		- _Sprinting_: el personaje esprintará todo el tiempo.
	- __Desired stance__:
		- _Standing_: el personaje estará de pie.
		- _Crouching_: el personaje estará agachado.
4. Configurar en la sección __State Values__ el equipamiento del personaje.
	- Si el __Overlay State__ está en _Default_ el personaje no equipará ningún objeto.
	- Si el __Overlay State__ está en _Rifle_ el personaje equipará un rifle.
	- Si el __Overlay State__ está en _Pistol 2H_ el personaje equipará una pistola a dos manos.
5. Dejar las secciones __Pawn__ con los valores por defecto.

<figure markdown>
  ![Personajes](imgs_personajes/char_12b.jpg){ width="700" }
  <figcaption>Configuración de la sección Input.</figcaption>
</figure>

<br>
---
## Configuración de NPCs

Se configurarán igual que los enemigos. Para que no puedan morir se recomienda poner una salud con un valor muy alto, por ejemplo, 1000000000.

<figure markdown>
  ![Personajes](imgs_personajes/char_01c.jpg){ width="300" }
  <figcaption>Configuración de la sección Input.</figcaption>
</figure>

<br>

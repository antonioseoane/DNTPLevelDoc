
Esta plantilla incluye varias mecánicas básicas y un personaje funcional que se pueden ver en un mapa de demostración.

Carácteríasticas de los personajes:

- Pueden andar, correr, esprintar, saltar, subirse a un lugar alto, agacharse, apuntar y disparar.
- Puede configurarse el color completo o por partes del juego.
- Puede configurarse el tipo de cuerpo, hombre o mujer.
- Puede configurarse la salud del personaje.
- Puede equipar un rifle o una pistola a dos manos.
- En el caso del personaje del jugador el rifle dispara ráfagas mientras que la pistola realiza disparos individuales.
- En el caso del personaje del jugador puede configurarse el daño que realiza al disparar.
- En el caso de los NPCs se puede configurar el comportamiento del movimiento y equipamiento de los personajes.

Mecánicas básicas:

- Objetos destructibles mediante disparos.
- Coleccionables que se recogen al colisionar.
- Teletransportador de un punto a otro.
- Cambio de nivel.

Otros elementos utilizados:
- Barreras para evitar que el personaje se caiga.
- Eventos implementados mediante secuencias cinemáticas dentro del nivel.
- Brushes para generar figuras complejas.

Además se incluyen distintos materiales configurables para crear un blockout del nivel que permiten añadir una grid de referencia a los objetos.

<br>

---
## Notas y avisos

- Dentro de la sección __PAWN__ de los actores de los personajes el actor que toma el papel del personaje del jugador, debe tener el __Auto Possess Player__ con el valor _Player 0_ y solo puede haber un actor que lo tenga así.
- En los __Project Settings__, dentro de las sección __Project - Maps&Modes__ el __Default GameMode__ debe tener asignado el valor __ALS_Antonio_GameMode_SP__ y en el __World Settings__ no debe de modificarse el __GameMode Override__.
- Errores de meshes y lighting. El personaje genera un warning en el cálculo de la iluminación. Esto es debido a un _StaticMesh_ que está sin asignar dentro del Blueprint. No se le debe dar importancia al aviso.

<br>
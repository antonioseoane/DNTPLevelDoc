
## Niveles de la plantilla

Nivel demo de la plantilla:
```
Content\AdvancedLocomotionV4\Levels\MapaInicio.umap
```

<figure markdown>
  ![Mapas](imgs_mapasdeejemplo/mapainicio.jpg){ width="700" }
  <figcaption>Mapa demo de la plantilla.</figcaption>
</figure>

Nivel utilizado por la demo para mostrar el cambio de nivel:
```
Content\AdvancedLocomotionV4\Levels\MapaDos.umap
```

<figure markdown>
  ![Mapas](imgs_mapasdeejemplo/mapados.jpg){ width="700" }   <figcaption>Segundo mapa para cambiar de nivel.</figcaption>
</figure>

Nivel vacío con el personaje principal para empezar a diseñar un nivel nuevo:
```
Content\AdvancedLocomotionV4\Levels\MapaVacio.umap
```

<figure markdown>
![Mapas](imgs_mapasdeejemplo/mapavacio.jpg){ width="700" }
  <figcaption>Mapa vacío.</figcaption>
</figure>

<br>

---
## Nivel demo de ALSV4

Mapa con ejemplo de las mecánicas del personaje del Advanced Locomotion System V4 (ALSV4):

```
Content\AdvancedLocomotionV4\Levels\ALS_DemoLevel.umap
```

<figure markdown>
  ![Mapas](imgs_mapasdeejemplo/mapaalsv4.jpg){ width="700" }
  <figcaption>Mapa demo de ALSV4.</figcaption>
</figure>

<br>


Esquema de control de la plantilla:

<figure markdown>
||Teclado/Ratón|Mando Xbox|
|--:|:--:|:--:|
|__Movimiento del personaje__|WASD|LEFT STICK|
|__Movimiento de la cámara__|Movimiento del Ratón|RIGHT STICK|
|__Apuntar__|Botón derecho del ratón|LEFT TRIGGER|
|__Disparar__|Botón izquierdo del ratón|RIGHT TRIGGER|
|__Modo andar__|CONTROL|RIGHT BUMPER / Y|
|__Sprintar__|SHIFT|LEFT BUMPER / LEFT STICK BUTTON|
|__Saltar__|ESPACIO|A|
|__Modo agachado__|C|B|
|__Enfundar/desenfundar__|Q|D-PAD DOWN|
|__Cambiar a pistola__|1|D-PAD LEFT|
|__Cambiar a rifle__|2|D-PAD RIGHT|
|__Cambiar cámara de hombro__|R|RIGHT STICK BUTTON|
</figure>

<br>

<figure markdown>
  ![Esquema mando Xbox](imgs_esquemadecontrol/360_controller.svg.png){ width="" }
  <figcaption>Esquema del mando de Xbox.</figcaption>
</figure>

<br>

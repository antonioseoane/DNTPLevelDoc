
## El actor de cambio de nivel

El __MapChanger_BP__ es un actor que cargar otro nivel cuando el personaje del jugador colisiona contra él. Este actor se encuentra en:

```
Content\Mechanics\MapChanger_BP.uasset
```

El volumen del actor de cambio de nivel se puede escalar para ajustar el volumen de colisión con el jugador.

!!! Note
	Cuando se cambie de nivel el personaje no se transporta, se utilizará el personaje del jugador que se encuentre situado en el nuevo nivel.

<figure markdown>
  ![Cambio de nivel](imgs_cambiodenivel/mapchanger_01.jpg){ width="500" }
  <figcaption>Actor de cambio de nivel.</figcaption>
</figure>


<br>

---

## Atributos

El __MapChanger_BP__ tiene los siguientes atributos que se encuentran en la sección __Default__:

<figure markdown>
|Atributo|Descripción|Valores|
|--|--|:--:|
|__Level__|Referencia a un nivel de Unreal.|Map|
</figure>

<figure markdown>
  ![Atributos de un destructible](imgs_cambiodenivel/mapchanger_02.jpg){ width="" }
  <figcaption>Atributos del actor de cambio de nivel.</figcaption>
</figure>

<br>

---
## Plantillas Unreal Engine :material-unreal:

- [DNTPLevel 2025-UE544 v4.0.0 (5/3/2025) :fontawesome-solid-file-arrow-down:](https://gitlab.com/antonioseoane/DNTPLevel/-/archive/v4.0.0/DNTPLevel-v4.0.0.zip)

- [DNTPLevel 2023-UE531 v3.0.0 (21/11/2023) :fontawesome-solid-file-arrow-down:](https://gitlab.com/antonioseoane/DNTPLevel/-/archive/v3.0.0/DNTPLevel-v3.0.0.zip)

- [DNTPLevel 2022-UE427 v2.0.0 (27/11/2022) :fontawesome-solid-file-arrow-down:](https://gitlab.com/antonioseoane/DNTPLevel/-/archive/v2.0.0/DNTPLevel-v2.0.0.zip)

<!-- - [DNTPLevel 2022-UE427 v2.0.0-Preview (25/11/2022) :fontawesome-solid-file-arrow-down:](https://gitlab.com/antonioseoane/DNTPLevel/-/archive/v2.0.0-Preview/DNTPLevel-v2.0.0-Preview.zip) -->


<!--<br>

---
## Plantilla con Third Person Character :material-unreal:

- [DNTPLevel 2022-UE427 v1.0.4 (16/11/2022) :fontawesome-solid-file-arrow-down:](https://gitlab.com/antonioseoane/DNTPLevel/-/archive/v1.0.4/DNTPLevel-v1.0.4.zip)

!!! Warning
    Versión antigua. No se recomienda su uso.-->

<br>


---
## Repositorio del proyecto en GitLab

[Repositorio :fontawesome-brands-gitlab:](https://gitlab.com/antonioseoane/DNTPLevel){ .md-button } [Otras releases](https://gitlab.com/antonioseoane/DNTPLevel/-/releases){ .md-button }


<br>

---
## Changelog

- v4.0.0
	- Actualizada la plantilla a Unreal Engine 5.4.4
	- Configurado el proyecto con Lumen
	- Añadidos nuevos mapas con ejemplos
	- Arreglado problema de color de los ojos cuando se cambia configuración
	- Se añade opción de matar directamente a un personaje al acercarse y pulsar E
	- Se crea una versión de cámara propia y se externaliza la tabla de configuración de la cámara

- v3.0.0
	- Actualizada la plantilla a Unreal Engine 5.3.1

- v2.0.0
	- Soporte completo para mando Xbox
	- Añadido personaje femenino

- v2.0.0-Preview
	- Versión previa de la plantilla con ALSV4

<br>

---
## Roadmap

- Top down Character
- Objectives BP and Widget
- Persistent data when jump between levels
- Enemy patrols
- Cinematic blueprint
- Killer doors
- Metahuman characters

<br>
